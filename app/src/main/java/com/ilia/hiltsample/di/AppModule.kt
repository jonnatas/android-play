package com.ilia.hiltsample.di

import com.ilia.hiltsample.data.remote.Api
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideApi(): Api {
        return Retrofit.Builder().baseUrl("https://test.com").build().create(Api::class.java)
    }

    @Provides
    @Singleton
    @Named("one")
    fun provideString1() = "Text 1"

    @Provides
    @Singleton
    @Named("two")
    fun provideString2() = "Text 2"
}