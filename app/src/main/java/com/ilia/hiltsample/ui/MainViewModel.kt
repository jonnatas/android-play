package com.ilia.hiltsample.ui

import androidx.lifecycle.ViewModel
import com.ilia.hiltsample.domain.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val mainRepository: MainRepository
) : ViewModel() {

}