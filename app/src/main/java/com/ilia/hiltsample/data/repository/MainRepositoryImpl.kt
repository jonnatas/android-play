package com.ilia.hiltsample.data.repository

import com.ilia.hiltsample.data.remote.Api
import com.ilia.hiltsample.domain.repository.MainRepository
import javax.inject.Inject

class MainRepositoryImpl @Inject constructor(
    private val api: Api
) : MainRepository {

    override suspend fun getSomething() {

    }

}