package com.ilia.hiltsample.data.remote

import retrofit2.http.GET

interface Api {

    @GET("test")
    suspend fun getSomething()
}